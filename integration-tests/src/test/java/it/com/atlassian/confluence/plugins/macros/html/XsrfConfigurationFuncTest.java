package it.com.atlassian.confluence.plugins.macros.html;

import com.atlassian.confluence.plugin.functest.util.ConfluenceBuildUtil;
import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.io.InputStream;
import java.util.Objects;
import java.util.Properties;

public class XsrfConfigurationFuncTest extends AbstractConfluencePluginWebTestCaseBase {
    private Properties properties;

    @Override
    protected void setUp() throws Exception {
        super.setUp();

        initDefaultProperties();
    }

    private void initDefaultProperties()
            throws IOException {
        properties = new Properties();
        InputStream in = null;

        try {
            in = getClass().getClassLoader().getResourceAsStream("com/atlassian/confluence/default.properties");
            properties.load(Objects.requireNonNull(in));
        } finally {
            IOUtils.closeQuietly(in);
        }
    }

    private int getBuildNumber() {
        return Integer.parseInt(properties.getProperty("build.number"));
    }

    public void testXsrf() {
        if (getBuildNumber() >= 1600 && !ConfluenceBuildUtil.containsSudoFeature())
            assertResourceXsrfProtected("/admin/plugins/whitelist/save.action");
    }
}
